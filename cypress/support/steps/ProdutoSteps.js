import produto from '../pageobjects/produto/ProdutoPage'


Given(/^o usuario esteja logado no site$/, () => {
	cy.backgroundLogin();
});

When(/^clicar em um produto$/, () => {
	produto.acessarProduto();
});

Then(/^devera ser redirecionado para a pagina do produto$/, () => {
	produto.validarPaginaProduto();
});
