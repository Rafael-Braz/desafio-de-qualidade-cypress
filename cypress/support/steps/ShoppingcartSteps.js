
import shoppingcart from '../pageobjects/shoppingcart/ShoppingcartPage'

// Scenario: Adicionar um item ao carrinho de compras
When(/^clicar no botao Add to Cart$/, () => {
	shoppingcart.adicionarItem();
});


Then(/^devera constar um item no carrinho de compras$/, () => {
    shoppingcart.validarItemAdicionado();
});

//Scenario: Remover um item do carrinho de compras
When(/^com um item no carrinho$/, () => {
	shoppingcart.adicionarItem();
});


When(/^clicar no botao Remover$/, () => {
	shoppingcart.removerItem();
});

Then(/^o item devera ser removido do carrinho$/, () => {
	shoppingcart.validarItemRemovido();
});

//Scenario: Remover um item pela pagina do carrinho de compras
When(/^acessar o meu carrinho de compras$/, () => {
	shoppingcart.acessarCarrinho();
});

When(/^clicar no botao Remove$/, () => {
	shoppingcart.removerItem();
});

Then(/^o item devera ser removido do carrinho$/, () => {
	shoppingcart.validarItemRemovido();
});
