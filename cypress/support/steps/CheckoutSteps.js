import shoppingcart from '../../support/pageobjects/shoppingcart/ShoppingcartPage'
import Checkout from '../../support/pageobjects/checkout/CheckoutPage';
Given(/^o usuario esteja logado no site$/, () => {
	cy.backgroundLogin();
});

Then(/^possuir um item no carrinho de compras$/, () => {
	shoppingcart.adicionarItem();
    shoppingcart.acessarCarrinho();
});

Then(/^clicar no botao Checkout$/, () => {
	Checkout.validarItemCheckout();
    Checkout.fazerCheckout();
});

Then(/^preencher suas informacoes$/, () => {
	Checkout.preencherDados();
    Checkout.Continuar();
});

Then(/^finalizar a compra$/, () => {
	Checkout.validarInformacoes();
});

Then(/^a compra é realizada com sucesso$/, () => {
	Checkout.validarSucessoCompra();
});
