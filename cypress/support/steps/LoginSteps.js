import Login from '../pageobjects/login/LoginPage'
//Validando o acesso com um usuario válido
Given(/^Que eu acesse o site$/, () => {
    cy.visit('/');
});

When(/^preencher os campos com um usuario e senha validos$/, () => {
    Login.preencherCamposLogin('standard_user', 'secret_sauce');
});

When(/^clicar no botão Login$/, () => {
	Login.clickarBotaoLogin();
});


Then(/^devera ser realizado o login na plataforma$/, () => {
	Login.validarLoginSucesso();
});


//Validando o acesso com um usuario bloqueado
Then(/^preencher os campos com um usuario e senha bloqueado$/, () => {
	Login.preencherCamposLogin('locked_out_user', 'secret_sauce');
});


Then(/^devera exibir uma mensagem informando que o usuario esta bloqueado.$/, () => {
	Login.validarLoginError();
});

//Validar Campo Username vazio
Then(/^Não preencher o campo Username$/, () => {
	Login.preencherPassword('secret_sauce');
});



Then(/^devera exibir uma mensagem informando um erro$/, () => {
	Login.validarCampoVazio();
});
//validar Campo Password vazio



Then(/^Não preencher o campo password$/, () => {
	Login.PreencherUsername('standard_user');
});

