export const ELEMENTS = {
    btnAddItem: 'button[ id = add-to-cart-sauce-labs-backpack ]',
    btnRemoveItem: 'button[ id = remove-sauce-labs-backpack ]',
    btnShoppingCart: 'span[ class = "shopping_cart_badge"]',
    AcessarCarrinho: 'div[ id = shopping_cart_container]'
}