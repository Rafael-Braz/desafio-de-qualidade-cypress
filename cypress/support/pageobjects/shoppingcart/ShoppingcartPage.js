const el = require('./ShoppingcartElements').ELEMENTS
class shoppingcart{
    adicionarItem(){
        cy.get(el.btnAddItem).click();       
    }
    removerItem(){
        cy.get(el.btnRemoveItem).click();
        
    }
    validarItemAdicionado(){
        cy.get(el.btnRemoveItem).should('contain', 'Remove');
        cy.get(el.btnShoppingCart).should('contain', '1'); 
    }
    validarItemRemovido(){
        cy.get(el.btnRemoveItem).should('not.exist');
        cy.get(el.btnShoppingCart).should('not.exist');
    }
    acessarCarrinho(){
        cy.get(el.AcessarCarrinho).click(); 
    }
} export default new shoppingcart();