const el = require('./LoginElements').ELEMENTS
class Login {

    preencherCamposLogin(username, password) {
        cy.get(el.username).type(username);
        cy.get(el.password).type(password);
    }
    PreencherUsername(username){
    
        cy.get(el.username).type(username);
    }
    preencherPassword(password){
        cy.get(el.password).type(password);
    }
    clickarBotaoLogin() {
        cy.get(el.btnLogin).click();
    }
    validarLoginSucesso(){
        cy.get('div[class = peek]').should('be.visible');
    }
    validarLoginError(){
        cy.get(el.peekicon).should('have.text', 'Epic sadface: Sorry, this user has been locked out.');
    }
    validarCampoVazio(){
        cy.get(el.peekicon).should('exist');
    }

} export default new Login();