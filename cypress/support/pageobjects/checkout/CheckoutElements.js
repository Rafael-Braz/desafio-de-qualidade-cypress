export const ELEMENTS = {
    itemCart: '.cart_item',
    btnCheckout: '#checkout',
    firstName: '#first-name',
    lastName: 'input[ data-test*= lastName ]',
    zipCode: 'input[ name*= Code]',
    btnContinue: '#continue',
    itemName: 'div.inventory_item_name',
    btnFinish: '#finish',
    checkoutComplete: 'h2.complete-header'

}