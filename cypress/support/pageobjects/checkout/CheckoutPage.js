
var Chance = require('chance');
var chance = new Chance();

const el = require('./CheckoutElements').ELEMENTS
class Chekout {
    

    validarItemCheckout() {
        cy.get(el.itemCart).should('exist');
    }
    fazerCheckout(){
        cy.get(el.btnCheckout).click();
    }
    preencherDados(){
        cy.get(el.firstName).type(chance.first());
        cy.get(el.lastName).type(chance.last());
        cy.get(el.zipCode).type(chance.zip());
        
    }
    Continuar(){
        cy.get(el.btnContinue).click();
    }
    validarInformacoes(){
        cy.get(el.itemName).should('have.text', 'Sauce Labs Backpack');
        cy.get(el.btnFinish).click();
    }
    validarSucessoCompra(){
        cy.get(el.checkoutComplete).should('have.text', 'THANK YOU FOR YOUR ORDER');
    }

} export default new Chekout();