const el = require('./ProdutoElements').ELEMENTS
class Produto{
    acessarProduto(){
        cy.get(el.btnProduto).click();
    }
    validarPaginaProduto(){
        cy.get(el.titleProduto).should('have.text', 'Sauce Labs Backpack');
    }
} export default new Produto();