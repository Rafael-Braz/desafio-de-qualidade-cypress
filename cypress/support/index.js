// ***********************************************************
// This example support/index.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************
Cypress.Commands.add('backgroundLogin', () =>{
    cy.visit('/');
        cy.get('input[ id = user-name]').type('standard_user');
        cy.get('input[ id = password]').type('secret_sauce');
        cy.get('input[ id= login-button]').click();

})