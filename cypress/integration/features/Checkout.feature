Feature: Checkout
    Como um usuario, desejo realizar uma compra com sucesso no site.

Scenario: Realizar o checkout do produto e finalizar a compra
Given o usuario esteja logado no site
And possuir um item no carrinho de compras
And clicar no botao Checkout
And preencher suas informacoes
And finalizar a compra
Then a compra é realizada com sucesso