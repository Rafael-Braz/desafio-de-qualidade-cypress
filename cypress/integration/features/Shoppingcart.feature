Feature: Shoppingcart
Como um usuario, gostaria de adicionar ou remover um item do meu carrinho de compras.


Scenario: Adicionar um item ao carrinho de compras pelo botão "Add to Cart"
Given o usuario esteja logado no site
When clicar no botao Add to Cart
Then devera constar um item no carrinho de compras

Scenario: Remover um item do carrinho de compras pelo botão "Remove"
Given o usuario esteja logado no site
And com um item no carrinho
When clicar no botao Remover
Then o item devera ser removido do carrinho

Scenario: Remover um item pela pagina do carrinho de compras
Given o usuario esteja logado no site
And com um item no carrinho
And acessar o meu carrinho de compras
When clicar no botao Remove
Then o item devera ser removido do carrinho


    