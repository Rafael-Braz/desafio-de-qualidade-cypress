Feature: Login
    
    Scenario: Realizar login com sucesso.
        Given   Que eu acesse o site
        And     preencher os campos com um usuario e senha validos
        And     clicar no botão Login
        Then    devera ser realizado o login na plataforma

    Scenario: Realizar login com um usuario bloqueado
        Given   Que eu acesse o site
        And     preencher os campos com um usuario e senha bloqueado
        And     clicar no botão Login
        Then    devera exibir uma mensagem informando que o usuario esta bloqueado.

    Scenario: Realizar login com o campo Username vazio
        Given   Que eu acesse o site
        And     Não preencher o campo Username
        And     clicar no botão Login
        Then    devera exibir uma mensagem informando um erro

        Scenario: Realizar login com o campo Password vazio
        Given   Que eu acesse o site
        And     Não preencher o campo password
        And     clicar no botão Login
        Then    devera exibir uma mensagem informando um erro