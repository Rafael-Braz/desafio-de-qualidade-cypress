****
**PRÉ-REQUISITOS**

>NodeJs instalado na máquina  
    Download: https://nodejs.org/en/ 

>Um editor de código:  
Recomendação: Visual Studio  
Download: https://code.visualstudio.com/download

****


**Para executar o teste como no exemplo, devemos executar o comando**

    npm run cy:open
- Em seguida clicar em All.features como na imagem a seguir:

![Relatório gerado](./documents/img/execucao/3.png)

 - Então deverá abrir uma aba do navegador, executando os testes.

![Relatório gerado](./documents/img/execucao/4.png)


**PASSO-A-PASSO PARA EXECUTAR OS TESTES COM RELATÓRIOS**

Abra o terminal
- execute o comando

        npm run cy:test

fazendo com que os testes automatizados sejam executados em modo headless(via terminal).

****
![Executando os testes](./documents/img/execucao/1.png)

- Em seguida, execute o comando:

        npm run cy:reports

para gerar o relatório conforme a imagem abaixo:

![Relatório gerado](./documents/img/execucao/2.png)
****

**Plugins utilizados**

>**Chance.js** para gerar dados aleatórios
https://chancejs.com/

> **Cucumber-html-reporter** para gerar os relatórios de execução dos   testes 
     https://github.com/gkushang/cucumber-html-reporter

 > **cypress-cucumber-preprocessor** para configurar o projeto e desenvolver os testes utilizando *Features* com **Cucumber**
    https://github.com/TheBrainFamily/cypress-cucumber-preprocessor#get-started
