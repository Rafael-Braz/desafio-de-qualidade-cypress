**** 
**RELATÓRIO DE BUGS ENCONTRADOS**
****
        CENÁRIO DE TESTE: CARRINHO DE COMPRAS
                
>Pré-Condições: Para realizar o teste, deverá estar logado com o usuário "standard_user”    
Login: standard_user   
Password: secret_sauce

Ao acessar a página de um determinado produto da loja, não é possível adicionar mais de um item do mesmo produto no carrinho, pois assim que clicamos no botão “Add to Cart” muda para “Remove".

![image info](./documents/img/bugs/bug1.png)
**Possível solução:** 
Criar um campo “quantidade” na modal do produto, assim o usuário conseguirá informar qual a quantidade desejada daquele item antes de chegar ao carrinho de compras e no carrinho de compras deve ser possível editar o campo “quantidade” do produto adicionado, pois na página do carrinho de compras já tem implementado um campo onde é informado a quantidade de um determinado item conforme na imagem abaixo: (QTY)

![image info](./documents/img/bugs/QTY.png)

****
      CENÁRIO DE TESTE: PRODUTO
>Pré-Condições: Para realizar o teste, deverá estar logado com o usuário "standard_user”    
Login: standard_user   
Password: secret_sauce

Alguns produtos estão exibindo possíveis métodos no título do produto e na descrição do item, conforme as imagens abaixo 

![image info](./documents/img/bugs/bug2.png)

**Possível solução:**  Implementar as correções necessárias para que o título ou descrição de um item seja exibido corretamente. 

**** 
     CENÁRIO DE TESTE: PRODUTO

>Pré-Condições: Para realizar o teste, deverá estar logado com o usuário "problem_user”    
Login: problem_user   
Password: secret_sauce
     

Na tela Products ao adicionar um produto em alguns momentos o botão “Remove” não funciona, fazendo com que não seja possível remover um produto clicando no botão.

![image info](./documents/img/bugs/Bug3.png)

Algumas vezes o botão “Add to Cart” não funciona como esperado e não adiciona o produto ao carrinho de compras. 

![image info](./documents/img/bugs/bug31.png)

**Possíveis solução:** Como esse erro não acontece com muita frequência o interessante seria realizar alguns testes unitários para verificar o comportamento isolado e identificar a causa raiz do erro e posteriormente a correção do mesmo. 

**** 

        CENÁRIO DE TESTE: CHECKOUT

>Pré-Condições: Para realizar o teste, deverá estar logado com o usuário "problem_user”    
Login: problem_user    
Password: secret_sauce

Na tela “Checkout: You information”, ao tentar realizar o checkout dos produtos e preencher o campo "Last Name", ele só permite 1 caráter e substitui o texto do Campo First Name fazendo com que não seja possível dar sequência no checkout. 

![image info](./documents/img/bugs/bug4.png)

**Possível solução:** Implementar correções e melhorias nos campos de formulário de modo que um campo não sobrescreva o conteúdo digitado em outro, para que todos os campos sejam preenchidos corretamente. 

****

        CENÁRIO DE TESTE: CHECKOUT

>Pré-Condições: Para realizar o teste, deverá estar logado com o usuário "standard_user”    
Login: standard_user    
Password: secret_sauce

Na tela “Checkout: You information”, não foi feito uma validação de caracteres permitidos e a quantidade mínima/máxima permitida nos campos “First Name”, “Last Name” e “ZipCode”, podendo inserir qualquer tipo de dados em todos os campos e uma quantidade inifita de caráteres. 

![image info](./documents/img/bugs/bug41.png)

**Possível solução:** implementar validações de dados inseridos nos campos “First Name”, “Last Name” e “ZipCode” além de limitar a quantidade de caracteres permitidos em cada campo de acordo com a necessidade de cada um. 

****

        CENÁRIO DE TESTE: CHECKOUT

>Pré-Condições: Para realizar o teste, deverá estar logado com o usuário "standard_user”    
Login: standard_user    
Password: secret_sauce

É possível finalizar uma compra mesmo com o carrinho vazio. 

![image info](./documents/img/bugs/Bug5.png)
![image info](./documents/img/bugs/Bug51.png)

**Possível solução:** implementar validações onde deverá verificar se existe pelo menos um item no carrinho. Caso não exista item(s) no carrinho de compras, ao clicar no botão Checkout não deverá permitir que o usuário prossiga para a próxima etapa, e informar o motivo pelo qual ele não consegue prosseguir com o checkout. 
****

        CENÁRIO DE TESTE: ACESSAR UM PRODUTO PELA URL

>Pré-Condições: Para realizar o teste, deverá estar logado com o usuário "standard_user”    
Login: standard_user    
Password: secret_sauce

Ao informar um id direto pela url é exibido na tela um produto “item not found” 
![image info](./documents/img/bugs/bug6.png)

![image info](./documents/img/bugs/Bug61.png)

É possível adicionar esse produto inexistente ao carrinho de compras. 

Ao acessar a página do carrinho com esse item adicionado, não é possível acessar a página, pois retorna toda branca conforme a imagem abaixo

![image info](./documents/img/bugs/bug62.png)
**Possíveis soluções:**

 - Não permitir que o usuário consiga acessar um item passando parâmetros pela url. 
- Quando o usuário pesquisar por um produto inexistente, exibir uma mensagem de erro em vez de gerar um “produto inválido” contendo métodos e funções de um produto válido. 

****
